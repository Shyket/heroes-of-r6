import { Component } from '@angular/core';
import { hero } from './services/Hero';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Heroes of R6';
  displayDetails: boolean = false;

  hero = {} as hero;

  showHeroDetails(hero: hero): void {
    this.displayDetails = true;
    this.hero = hero;
  }

  hideHeroDetails(): void {
    this.displayDetails = false;
  }
}
