import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { hero } from '../../services/Hero';
@Component({
  selector: 'app-hero-description',
  templateUrl: './hero-description.component.html',
  styleUrls: ['./hero-description.component.scss'],
})
export class HeroDescriptionComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  @Output() hideHeroDetails = new EventEmitter();

  getBackgroundColor(power: number): string {
    if (power >= 80) {
      return '	#a70000';
    } else if (60 <= power && power <= 79) {
      return '#ff0000';
    } else if (40 <= power && power <= 69) {
      return '#ff5252';
    } else if (20 <= power && power <= 59) {
      return '#ff7b7b';
    }
    return '#ffbaba';
  }

  cancelHandler(): void {
    this.hideHeroDetails.emit();
  }

  @Input() hero: hero = {} as hero;

}
