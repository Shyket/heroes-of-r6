import { HeroService } from './../../services/hero-service.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { hero } from "../../services/Hero"
@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss'],
})
export class HeroListComponent implements OnInit {
  constructor(private heroService: HeroService) {

  }

  ngOnInit(): void {}

  @Output() showHeroDetails = new EventEmitter();

  heroList:hero[] = this.sortHeroList(this.heroService.getHeroes());

  sortHeroList(heroes: hero[]) {
    heroes.sort((a, b) => {
      return b.power - a.power;
    });
    return heroes;
  }

  heroClickHandler(hero: hero): void {
    this.showHeroDetails.emit(hero);
  }
}
