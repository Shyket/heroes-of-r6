import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroListComponent } from './components/hero-list/hero-list.component';
import { HeroDescriptionComponent } from './components/hero-description/hero-description.component';
import { HeroHeaderComponent } from './hero-header/hero-header.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroListComponent,
    HeroDescriptionComponent,
    HeroHeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
