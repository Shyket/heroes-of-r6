type Nullable<T> = T | null;
export interface hero {
  id: number;
  name: string;
  nationality: string;
  power: number;
  ability: string;
  description: string;
  role: string[];
  health: 1 | 2 | 3;
  speed: 1 | 2 | 3;
  difficulty: 1 | 2 | 3;
  dateOfHeroism: Date;
}
